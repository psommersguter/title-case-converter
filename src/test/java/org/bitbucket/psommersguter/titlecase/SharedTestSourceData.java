package org.bitbucket.psommersguter.titlecase;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

public class SharedTestSourceData {

    /**
     * Provides a Stream of Arguments of test values for title case conversion.
     * <p/>
     * The values are taken from the <a href=https://github.com/ap/titlecase/blob/master/test.pl>test suite</a> of
     * <a href=http://plasmasturm.org/code/titlecase/>http://plasmasturm.org/code/titlecase/</a>.
     *
     * @return A stream of values to be used in a {@link ParameterizedTest}
     */
    public static Stream<Arguments> allTitleCaseTestArguments() {
        return Stream.of(
            Arguments.of("For step-by-step directions email someone@gmail.com", "For Step-by-Step Directions Email someone@gmail.com"),
            Arguments.of("2lmc Spool: 'Gruber on OmniFocus and Vapo(u)rware'", "2lmc Spool: 'Gruber on OmniFocus and Vapo(u)rware'"),
            Arguments.of("Have you read “The Lottery”?", "Have You Read “The Lottery”?"),
            Arguments.of("your hair[cut] looks (nice)", "Your Hair[cut] Looks (Nice)"),
            Arguments.of("People probably won't put http://foo.com/bar/ in titles", "People Probably Won't Put http://foo.com/bar/ in Titles"),
            Arguments.of("Scott Moritz and TheStreet.com’s million iPhone la-la land", "Scott Moritz and TheStreet.com’s Million iPhone La-La Land"),
            Arguments.of("BlackBerry vs. iPhone", "BlackBerry vs. iPhone"),
            Arguments.of("Notes and observations regarding Apple’s announcements from ‘The Beat Goes On’ special event", "Notes and Observations Regarding Apple’s Announcements From ‘The Beat Goes On’ Special Event"),
            Arguments.of("Read markdown_rules.txt to find out how _underscores around words_ will be interpretted", "Read markdown_rules.txt to Find Out How _Underscores Around Words_ Will Be Interpretted"),
            Arguments.of("Q&A with Steve Jobs: 'That's what happens in technology'", "Q&A With Steve Jobs: 'That's What Happens in Technology'"),
            Arguments.of("What is AT&T's problem?", "What Is AT&T's Problem?"),
            Arguments.of("Apple deal with AT&T falls through", "Apple Deal With AT&T Falls Through"),
            Arguments.of("this v that", "This v That"),
            Arguments.of("this vs that", "This vs That"),
            Arguments.of("this v. that", "This v. That"),
            Arguments.of("this vs. that", "This vs. That"),
            Arguments.of("The SEC's Apple probe: what you need to know", "The SEC's Apple Probe: What You Need to Know"),
            Arguments.of("'by the way, small word at the start but within quotes.'", "'By the Way, Small Word at the Start but Within Quotes.'"),
            Arguments.of("Small word at end is nothing to be afraid of", "Small Word at End Is Nothing to Be Afraid Of"),
            Arguments.of("Starting sub-phrase with a small word: a trick, perhaps?", "Starting Sub-Phrase With a Small Word: A Trick, Perhaps?"),
            Arguments.of("Sub-phrase with a small word in quotes: 'a trick, perhaps?'", "Sub-Phrase With a Small Word in Quotes: 'A Trick, Perhaps?'"),
            Arguments.of("Sub-phrase with a small word in quotes: \"a trick, perhaps?\"", "Sub-Phrase With a Small Word in Quotes: \"A Trick, Perhaps?\""),
            Arguments.of("\"Nothing to Be Afraid of?\"", "\"Nothing to Be Afraid Of?\""),
            Arguments.of("a thing", "A Thing"),
            Arguments.of("Dr. Strangelove (or: how I Learned to Stop Worrying and Love the Bomb)", "Dr. Strangelove (Or: How I Learned to Stop Worrying and Love the Bomb)"),
            Arguments.of("  this is trimming", "This Is Trimming"),
            Arguments.of("this is trimming  ", "This Is Trimming"),
            Arguments.of("  this is trimming  ", "This Is Trimming"),
            Arguments.of("IF IT’S ALL CAPS, FIX IT", "If It’s All Caps, Fix It"),
            Arguments.of("What could/should be done about slashes?", "What Could/Should Be Done About Slashes?"),
            Arguments.of("Never touch paths like /var/run before/after /boot", "Never Touch Paths Like /var/run Before/After /boot"),
            Arguments.of("a", "A"),
            Arguments.of("", "")
        );
    }

}
