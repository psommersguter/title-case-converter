package org.bitbucket.psommersguter.titlecase.java;

import org.bitbucket.psommersguter.titlecase.SharedTestSourceData;
import org.bitbucket.psommersguter.titlecase.TitleCaseConverter;
import org.bitbucket.psommersguter.titlecase.java.JavaTitleCaseConverter;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JavaTitleCaseConverterTest {

    private static TitleCaseConverter titleCaseConverter = new JavaTitleCaseConverter.Builder()
        .withDefaults()
        .build();

    @ParameterizedTest
    @MethodSource("conversionTestArguments")
    void testConvertToTitleCase(String input, String expected) {
        assertEquals(expected, titleCaseConverter.convert(input));
    }

    private static Stream<Arguments> conversionTestArguments() {
        return SharedTestSourceData.allTitleCaseTestArguments();
    }

}
