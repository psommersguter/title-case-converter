package org.bitbucket.psommersguter.titlecase.java;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.UrlValidator;
import org.bitbucket.psommersguter.titlecase.TitleCaseConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.IntStream;

public class JavaTitleCaseConverter implements TitleCaseConverter {

    private static final Logger log = LoggerFactory.getLogger(JavaTitleCaseConverter.class);

    /**
     * A list of words that should be kept in lower case.
     */
    private final List<String> smallWords;
    private final List<String> keepAsIsWords;
    private final Map<String, String> explicitConversions;
    private final boolean trim;

    private JavaTitleCaseConverter(Builder builder) {
        this.smallWords = Collections.unmodifiableList(builder.smallWords);
        this.keepAsIsWords = Collections.unmodifiableList(builder.keepAsIsWords);
        this.explicitConversions = Collections.unmodifiableMap(builder.explicitConversions);
        this.trim = builder.trim;
    }

    private static ConversionContext createConversionContext(String[] items, int listIndex) {
        //Special handling for first element
        if (listIndex == 0) {
            return new ConversionContext("", items[0]);
        }
        return new ConversionContext(items[listIndex - 1], items[listIndex]);
    }

    private static int countSpecialCharactersAtBeginning(String item) {
        int specialCharAtBeginningCount = 0;
        List<Character> specialCharacters = Arrays.asList('_', '[', '(', '\'', '"', '“', '”', '‘', '’');
        for (int i = 0; i < item.length(); i++) {
            if (specialCharacters.contains(item.charAt(i))) {
                specialCharAtBeginningCount++;
            } else {
                break;
            }
        }
        return specialCharAtBeginningCount;
    }

    private static boolean indicatesSubPhrase(String item) {
        //When a word ends with a colon, probably a sub phrase starts
        return item.endsWith(":");
    }

    private static boolean isUrl(String item) {
        return UrlValidator.getInstance().isValid(item);
    }

    private static boolean isEmailAddress(String item) {
        return EmailValidator.getInstance().isValid(item);
    }

    private static String characterToUpperCase(String item, int upperCasePosition) {
        return item.substring(0, upperCasePosition) +
            item.substring(upperCasePosition, upperCasePosition + 1).toUpperCase() +
            item.substring(upperCasePosition + 1).toLowerCase();
    }

    /**
     * Calculates the probability of the given input to be a file name
     *
     * @param item The String to check
     * @return Number from 0 to 100
     */
    private static int getFileNameProbability(String item) {
        //TODO use list from https://fileinfo.com/filetypes/common for file extensions
        String[] knownFileExtensions = new String[]{"txt", "pdf", "docx", "csv"};
        for (String extension : knownFileExtensions) {
            if (item.endsWith("." + extension)) {
                return 100;
            }
        }

        String[] split = item.split(".");
        if (split.length < 2) {
            return 0;
        }

        String extension = split[1];
        if (extension.length() >= 3 && extension.length() <= 5) {
            return 90;
        }
        return 0;
    }

    private static String joinToSingleString(String[] items, String delimiter) {
        StringJoiner joiner = new StringJoiner(delimiter);
        for (String string : items) {
            joiner.add(string);
        }
        return joiner.toString();
    }

    private static String[] splitBySpace(String input, boolean trim) {
        final String splitRegex = " ";
        String[] spaceSplitArray;
        if (trim) {
            spaceSplitArray = input.trim().split(splitRegex);
        } else {
            spaceSplitArray = input.split(splitRegex);
        }
        return spaceSplitArray;
    }

    private static boolean shouldSkipConversion(String item, List<String> smallWords, List<String> keepAsIsWords) {
        return item.length() < 1 ||
            smallWords.contains(item) ||
            smartlyKeepAsIs(item, keepAsIsWords) ||
            isEmailAddress(item) ||
            isUrl(item) ||
            getFileNameProbability(item) >= 90;
    }

    /**
     * Checks whether a word should be kept as it is.
     * <br/>
     * The check is smart enough to also keep slight variations of words.
     *
     * @param item The word to check against list of words to keep as is
     * @return true when the word should stay as is, false otherwise
     */
    private static boolean smartlyKeepAsIs(String item, List<String> keepAsIsWords) {
        String check = item;
        if (item.endsWith("'s") || item.endsWith("’s")) {
            check = item.substring(0, item.length() - 2);
        }
        return keepAsIsWords.contains(check);
    }

    @Override
    public String convert(String input) {
        String[] spaceSplitArray = splitBySpace(input, trim);
        String[] convertedItems = convertArray(spaceSplitArray);
        return joinToSingleString(convertedItems, " ");
    }

    private String[] convertArray(final String[] items) {
        //positional rules
        convertSmallWordAtPosition(0, items, keepAsIsWords);
        convertSmallWordAtPosition(items.length - 1, items, keepAsIsWords);

        //we iterate over the array with a parallel stream to gain performance
        return IntStream.range(0, items.length)
            .parallel()
            .mapToObj(i -> createConversionContext(items, i))
            .map(this::objectConvert)
            .toArray(String[]::new);
    }

    private String objectConvert(ConversionContext conversionContext) {
        String elementToConvert = conversionContext.getItem();
        if (indicatesSubPhrase(conversionContext.getLeftNeighbour())) {
            return findConversion(elementToConvert);
        }

        if (shouldSkipConversion(elementToConvert, smallWords, keepAsIsWords)) {
            log.trace("Skipping conversion for '{}'", elementToConvert);
            return elementToConvert;
        }

        return findConversion(elementToConvert);
    }

    private void convertSmallWordAtPosition(int position, String[] wordList, List<String> keepAsIsWords) {
        String wordToCheck = wordList[position];
        log.trace("Checking word {} at position {}", wordToCheck, position);
        if (smallWords.contains(wordToCheck) && !smartlyKeepAsIs(wordToCheck, keepAsIsWords)) {
            log.trace("Converting small word because of position{}", wordToCheck);
            wordList[position] = characterToUpperCase(wordToCheck, 0);
        }
    }

    private String findConversion(String item) {
        //check if there is an explicit conversion configured
        if (explicitConversions.containsKey(item.toLowerCase())) {
            String explicitConversion = explicitConversions.get(item.toLowerCase());
            log.trace("Explicitly converted '{}' to '{}'", item, explicitConversion);
            return explicitConversion;
        }

        //count special characters at the beginning of the item and convert the first 'real' character
        int specialCharAtBeginningCount = countSpecialCharactersAtBeginning(item);
        log.trace("Counted {} special characters at beginning of {}", specialCharAtBeginningCount, item);
        if (specialCharAtBeginningCount > 0) {
            String specialCharacterConversion = characterToUpperCase(item, specialCharAtBeginningCount);
            log.trace("Performed special character conversion from '{}' to '{}'", item, specialCharacterConversion);
            return specialCharacterConversion;
        }

        //if the contains slashes, we have to do some special handling for phrases like 'before/after' or 'could/should'
        if (item.contains("/")) {
            if (item.startsWith("/")) {
                //it's probably a path, do not touch
                log.trace("Did not touch {} because it likely is a path", item);
                return item;
            }
            //word contains a / so it could be a path or a mix of 2 words like could/should that must be capitalized
            String[] convertedElements = convertArray(item.split("/"));
            String wordWithSlashConversion = joinToSingleString(convertedElements, "/");
            log.trace("Converted word containing slashes from '{}' to '{}'", item, wordWithSlashConversion);
            return wordWithSlashConversion;
        }

        String defaultConversion = characterToUpperCase(item, 0);
        log.trace("Default conversion from '{}' to '{}'", item, defaultConversion);
        return defaultConversion;
    }

    @SuppressWarnings({"WeakerAccess", "UnusedReturnValue"})
    public static class Builder {
        private final List<String> smallWords;
        private final List<String> keepAsIsWords;
        private final Map<String, String> explicitConversions;
        private boolean trim;

        public Builder() {
            smallWords = new ArrayList<>();
            keepAsIsWords = new ArrayList<>();
            explicitConversions = new HashMap<>();
        }

        public Builder withDefaults() {
            explicitConversion("step-by-step", "Step-by-Step");
            explicitConversion("la-la", "La-La");
            explicitConversion("sub-phrase", "Sub-Phrase");

            // "from", ??
            // "with", ??
            // "out", ??
            // "about", ??
            // "like", ??
            smallWords(
                "a", "abaft", "above", "afore", "after", "along", "amid", "among", "an", "apud", "as", "aside", "at",
                "atop", "below", "but", "by", "circa", "down", "for", "given", "in", "into", "lest", "mid", "midst",
                "minus", "near", "next", "of", "off", "on", "onto", "over", "pace", "past", "per", "plus", "pro", "qua",
                "round", "sans", "save", "since", "than", "thru", "till", "times", "to", "under", "until", "unto", "up",
                "upon", "via", "vice", "worth", "the", "and", "nor", "or", "yet", "so", "vs", "vs.", "v", "v."
            );

            keepAllAsIs(
                "iPhone",
                "SEC",
                "AT&T", //American phone and internet provider
                "Q&A", //Abbreviation for "Questions and Answers"
                "BlackBerry", //Company that used to make phones
                "TheStreet.com", //Some news website with a stylized name for some reason
                "OmniFocus",
                "‘The Beat Goes On’" //Some Apple event
            );

            trim(true);
            return this;
        }

        public Builder explicitConversion(String source, String target) {
            this.explicitConversions.put(source, target);
            return this;
        }

        public Builder smallWords(String... smallWords) {
            this.smallWords.addAll(Arrays.asList(smallWords));
            return this;
        }

        public Builder smallWord(String word) {
            this.smallWords.add(word);
            return this;
        }

        public Builder keepAllAsIs(String... keepAsIsWords) {
            this.keepAsIsWords.addAll(Arrays.asList(keepAsIsWords));
            return this;
        }

        public Builder keepAsIs(String word) {
            this.keepAsIsWords.add(word);
            return this;
        }

        public Builder trim(boolean trim) {
            this.trim = trim;
            return this;
        }

        public JavaTitleCaseConverter build() {
            return new JavaTitleCaseConverter(this);
        }
    }
}
