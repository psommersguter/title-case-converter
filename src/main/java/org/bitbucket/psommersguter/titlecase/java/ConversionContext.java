package org.bitbucket.psommersguter.titlecase.java;

/**
 * Wrapper to contain additional information relevant for conversion.
 */
class ConversionContext {
    private final String leftNeighbour;
    private final String item;

    ConversionContext(String leftNeighbour, String item) {
        this.leftNeighbour = leftNeighbour;
        this.item = item;
    }

    public String getLeftNeighbour() {
        return leftNeighbour;
    }

    public String getItem() {
        return item;
    }

}
