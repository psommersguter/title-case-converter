package org.bitbucket.psommersguter.titlecase.javascript;

import org.bitbucket.psommersguter.titlecase.TitleCaseConverter;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

public class DavidGouchJavascriptConverter implements TitleCaseConverter {

    private JavaScriptConverter javaScriptConverter;

    public DavidGouchJavascriptConverter() {
        try {
            URL scriptUrl = new URL("http://individed.com/code/to-title-case/js/to-title-case.js");

            //Workaround because the downloaded script contains a BOM which leads to parsing errors when the
            //file.encoding property is not set to UTF-8 (default on Windows is Cp1252)
            UnicodeBOMInputStream unicodeBOMInputStream = new UnicodeBOMInputStream(scriptUrl.openStream());
            unicodeBOMInputStream.skipBOM();

            Reader reader = new InputStreamReader(unicodeBOMInputStream);
            javaScriptConverter = new JavaScriptConverter(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String convert(String input) {
        return javaScriptConverter.convert(input);
    }
}
