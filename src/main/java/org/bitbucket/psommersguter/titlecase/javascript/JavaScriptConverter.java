package org.bitbucket.psommersguter.titlecase.javascript;

import org.bitbucket.psommersguter.titlecase.TitleCaseConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.Reader;
import java.time.Duration;
import java.time.Instant;

public class JavaScriptConverter implements TitleCaseConverter {

    private static final Logger log = LoggerFactory.getLogger(JavaScriptConverter.class);

    private final ScriptEngine scriptEngine;

    public JavaScriptConverter(Reader convertFunctionScriptReader) {
        ScriptEngineManager manager = new ScriptEngineManager();
        this.scriptEngine = manager.getEngineByName("nashorn");
        loadBaseFunctionScript(convertFunctionScriptReader);
    }

    private void loadBaseFunctionScript(Reader scriptReader) {
        try {
            Instant startTime = Instant.now();

            scriptEngine.eval(scriptReader);
            Duration initDuration = Duration.between(startTime, Instant.now());
            log.debug("Function initialization took {} milliseconds", initDuration.toMillis());
        } catch (ScriptException e) {
            log.error("Failed to evaluate script providing .toTitleCase() function!", e);
            throw new RuntimeException("FAILED TO INITIALIZE", e);
        }
    }

    @Override
    public String convert(String input) {
        try {
            log.debug("Starting javascript conversion");
            Instant startTime = Instant.now();

            String escapedInput = input.replace("\"", "\\\"");
            log.debug("Passing escaped input >{}< to engine", escapedInput);

            Object scriptResult = scriptEngine.eval("\"" + escapedInput + "\".toTitleCase()");
            String stringResult = String.valueOf(scriptResult);
            String trimmedResult = stringResult.trim();

            Duration conversionDuration = Duration.between(startTime, Instant.now());
            log.debug("Conversion took {} milliseconds", conversionDuration.toMillis());
            return trimmedResult;
        } catch (ScriptException e) {
            log.error("Script error while converting with javascript.", e);
            return "ERROR DURING CONVERSION";
        }
    }
}
