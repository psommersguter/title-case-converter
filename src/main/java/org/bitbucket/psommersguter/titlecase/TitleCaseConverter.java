package org.bitbucket.psommersguter.titlecase;

public interface TitleCaseConverter {

    String convert(String input);
}
